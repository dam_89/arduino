/*
 * INDOAMERICA 2A - INGENIERIA INDUSTRIAL
 */

/**********************************************************************************CONFIGURACIÓN LCD*************************************************************************
  The circuit:
 * LCD VSS: pin to ground
 * LCD VDD: pin to 5V
 * LCD V0:  conectar al terminal central del potenciometro
 * LCD RS:  pin to digital pin 12
 * LCD R/W: pin to ground
 * LCD E:   pin to digital pin 11
 * LCD D0:  nada
 * LCD D1:  nada
 * LCD D2:  nada
 * LCD D3:  nada
 * LCD D4:  pin to digital pin 13
 * LCD D5:  pin to digital pin 8
 * LCD D6:  pin to digital pin 9
 * LCD D7:  pin to digital pin 10
 * LCD A:   conectar a un extremo de una resistencia de 220 ohms y el otro extremo de la resistencia a 5V
 * LCD K:   GND */






/*******************************************************************LIBRERIAS Y VARIABLES***************************************************************************************/
#include <OneWire.h>
#include <LiquidCrystal.h>
#include <Math.h>

  //Procedimientos especiales
  OneWire ds(35); //pin digital 35 de arduino conectado al pin central del sensor dallas
  LiquidCrystal lcd(12, 11, 13, 8, 9, 10);    // lcd(RS,E,D4,D5,D6,D7);

  //Declaración de variables
  int n;
  byte pin_final_de_carrera=53;
  byte pin_reset_contador=52;
  byte pin_encerar_encoder=40;
  byte pin_valor_encoder=41;
  byte b1; //bandera final de carrera
  byte b2; //bandera de lectura del sensor de temp dallas
  byte b3, b4; //banderas para encoder
  byte f;  //bandeRa de tiempo de presentación inicial de caratula 
  int t_timer; // periodo de tiempo de envio de datos puerto serial
  int t_caratula; //tiempo de caratula en milisegundos
  int periodo; //periodo de medición del sensor mínimo 1000 ms = 1 segundo
  unsigned long m1,m2,m3,m4; //medidas axuliares de tiempo
  float angulo_horizontal; //valor del angulo en grados sexagesimales
  float angulo_biela; //valor del angulo en grados sexagesimales  
  float angulo_encoder; //valor del angulo del disco del encoder en grados sexagesimales
  float altura,manivela,biela; //variables del sistema mecanico en milimetros
  float temporal; //lcd.print del angulo
  long auxiliar; //lcd.print del angulo

  //Declaración de variables OneWire Dallas Temperatura
  byte i;
  byte present = 0;
  byte data[12];
  byte addr[8];
  int HighByte, LowByte, TReading, SignBit, Tc_100, Whole, Fract;


/******************************************************************************* SETUP ***************************************************************************************/
void setup(){

  Serial.begin(9600);
  
  //Unidad - display 7seg
  pinMode(23, OUTPUT); //a 
  pinMode(22, OUTPUT); //b
  pinMode( 2, OUTPUT); //c
  pinMode( 3, OUTPUT); //d
  pinMode( 4, OUTPUT); //e
  pinMode(24, OUTPUT); //f
  pinMode(25, OUTPUT); //g

  //Decena - display 7seg
  pinMode(27, OUTPUT); //a  
  pinMode(26, OUTPUT); //b
  pinMode( 5, OUTPUT); //c
  pinMode( 6, OUTPUT); //d
  pinMode( 7, OUTPUT); //e
  pinMode(29, OUTPUT); //f
  pinMode(28, OUTPUT); //g

  //Entradas
  pinMode(pin_final_de_carrera,INPUT); //final de carrera
  pinMode(pin_reset_contador,INPUT); //reset de numero del contador
  pinMode(pin_encerar_encoder,INPUT); //encerar angulo_horizontal
  pinMode(pin_valor_encoder,INPUT); //valor del encoder 

  //LCD
  lcd.begin(16, 2);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Proy. Integrador");
  lcd.setCursor(0, 1);
  lcd.print(" 2A - INDUSTRIAL");
  
  //inicialización valores de parametros-----------------------------------
  n=0;
  b1=1; 
  b2=0;
  b3=digitalRead(pin_valor_encoder);
  b4=b3;
  f=0;
  display_contador(0);
  periodo=1000;
  m1=0;
  m2=0;
  m3=0;
  m4=0;

  //valores manipulables----------------------------------------------------
  t_timer=1000; // en milisegundos
  t_caratula=4000;  // en milisegundos
  angulo_encoder=5; // en grados
  angulo_horizontal=0; // en grados
  angulo_biela=0; // en grados
  altura=220; //en MM milimetros
  manivela=115; //en MM milimetros
  biela=600; //en MM milimetros  
}





/********************************************************************** FUNCIONES ***************************************************************************************/

void d_unidad(byte a, byte b, byte c, byte d, byte e, byte f, byte g) //salidas del display unidad
{
  digitalWrite (23,a);   
  digitalWrite (22,b);   
  digitalWrite ( 2,c);
  digitalWrite ( 3,d);
  digitalWrite ( 4,e);
  digitalWrite (24,f);
  digitalWrite (25,g);
}

void d_decena(byte a, byte b, byte c, byte d, byte e, byte f, byte g) //salidas del display decena
{
  digitalWrite (27,a);   
  digitalWrite (26,b);   
  digitalWrite ( 5,c);
  digitalWrite ( 6,d);
  digitalWrite ( 7,e);
  digitalWrite (29,f);
  digitalWrite (28,g);
}

void display_contador(int num)// Actualizar un valor "num" en el display
{
  byte decena=0, unidad=0;  
  decena=(byte)(num/10);
  unidad=num-decena*10;

  //Unidad
  switch (unidad){
        case 0: d_unidad(1,1,1,1,1,1,0); break;
        case 1: d_unidad(0,1,1,0,0,0,0); break;
        case 2: d_unidad(1,1,0,1,1,0,1); break;                    
        case 3: d_unidad(1,1,1,1,0,0,1); break;            
        case 4: d_unidad(0,1,1,0,0,1,1); break;            
        case 5: d_unidad(1,0,1,1,0,1,1); break;            
        case 6: d_unidad(1,0,1,1,1,1,1); break;            
        case 7: d_unidad(1,1,1,0,0,0,0); break;            
        case 8: d_unidad(1,1,1,1,1,1,1); break;            
        case 9: d_unidad(1,1,1,0,0,1,1); break;            
        default: break;
  }

  //Decena
  switch (decena){
        case 0: d_decena(1,1,1,1,1,1,0); break;
        case 1: d_decena(0,1,1,0,0,0,0); break;
        case 2: d_decena(1,1,0,1,1,0,1); break;                    
        case 3: d_decena(1,1,1,1,0,0,1); break;            
        case 4: d_decena(0,1,1,0,0,1,1); break;            
        case 5: d_decena(1,0,1,1,0,1,1); break;            
        case 6: d_decena(1,0,1,1,1,1,1); break;            
        case 7: d_decena(1,1,1,0,0,0,0); break;            
        case 8: d_decena(1,1,1,1,1,1,1); break;            
        case 9: d_decena(1,1,1,0,0,1,1); break;            
        default: break;
  }
}



/************************************************************************* LOOP *****************************************************************************************/
void loop(){ 

  //inicialización variables temperatura (OJO: Solo se ejecuta una vez)-----------------------------------
  ds.reset_search();
  if ( !ds.search(addr)) {
      ds.reset_search();
      return;
  }    
  if ( OneWire::crc8( addr, 7) != addr[7]) {return;}    
  ds.reset();
  ds.select(addr);
  ds.write(0x44,1); // start conversion, with parasite power on at the end
  


// do-while Se ejecuta infinitamente
do{
    

    //final de carrera-----------------------------------------------------------------------------------------------------------------------

    if (b1==1 && digitalRead(pin_final_de_carrera) == HIGH){
      delay(30); //antirebote
      b1=0;
      n++;
      if(n==100){n=0;}      
      display_contador(n);
    }  
    if (b1==0 && digitalRead(pin_final_de_carrera) == LOW){ b1=1;}




    //reset del contador-----------------------------------------------------------------------------------------------------------------------
    if( digitalRead(pin_reset_contador) == HIGH){
      n=0;  
      display_contador(n); 
    }




    //Sensor de Temperatura DALLAS--------------------------------------------------------------------------------------------------------------
    if(b2==1 && f==1){
                      b2=0;
                      present = ds.reset();
                      ds.select(addr);    
                      ds.write(0xBE);         // Read Scratchpad
                      for ( i = 0; i < 9; i++) {           // we need 9 bytes
                        data[i] = ds.read();
                      }
                      OneWire::crc8( data, 8);
                      LowByte = data[0];
                      HighByte = data[1];
                      TReading = (HighByte << 8) + LowByte;
                      SignBit = TReading & 0x8000;  // test most sig bit
                      if (SignBit) // negative
                      {
                        TReading = (TReading ^ 0xffff) + 1; // 2's comp
                      }
                      Tc_100 = (6 * TReading) + TReading / 4;    // multiply by (100 * 0.0625) or 6.25    
                      Whole = Tc_100 / 100;  // separate off the whole and fractional portions
                      Fract = Tc_100 % 100;    
                      lcd.setCursor(0, 1);
                      lcd.print("Temp.: ");
                      if (SignBit) // If its negative
                      {
                         lcd.print("-");
                      }
                      lcd.print(Whole);
                      lcd.print(".");
                      if (Fract < 10)
                      {
                         lcd.print("0");
                      }
                      lcd.print(Fract);
                      lcd.print((char)223); lcd.print("C  "); 
                
                      ////////----------------      
                      ds.reset_search();
                      if ( !ds.search(addr)) {
                          ds.reset_search();
                          return;
                      }    
                      if ( OneWire::crc8( addr, 7) != addr[7]) {return;}    
                      ds.reset();
                      ds.select(addr);
                      ds.write(0x44,1);         // start conversion, with parasite power on at the end
      
    }

    m1=(unsigned long)(millis()/periodo);
    if(m1!=m2){m2=m1; b2=1;}


    //SERIAL------------------------------------------------------------------------------------------------------
    m3=(unsigned long)(millis()/t_timer);
    if(m3!=m4){
      m4=m3;
      Serial.print("Proyecto Integrador - Ingenieria Industrial 2A \n");
      Serial.print("Angulo Biela-Horizontal:     "); Serial.println(angulo_biela,2); 
      Serial.print("Temperatura Sensor Dallas:   "); 

      if (SignBit) // If its negative
      {
         Serial.print("-");
      }
      Serial.print(Whole);
      Serial.print(".");
      if (Fract < 10)
      {
         Serial.print("0");
      }
      Serial.print(Fract);
      Serial.print("\nContador Final de Carrera:   "); Serial.print(n);
      Serial.print("\n\n");     
      
    }





    //ENCODER-------------------------------------------------------------------------------------------------
    b3=digitalRead(pin_valor_encoder); //lee el valor 1 o 0 del disco encoder
    if(b3!=b4 && f==1){
      b4=b3;
      angulo_horizontal+=angulo_encoder;
      if(angulo_horizontal>=360){angulo_horizontal=0;}

      //calculo angulo alfa ó angulo de la biela
      angulo_biela=asin((altura+manivela*sin(angulo_horizontal*3.141592/180))/biela)*180/3.141592;
      
      lcd.setCursor(0, 0);
      lcd.print("Alfa: ");
      auxiliar=(long)angulo_biela;
      lcd.print(auxiliar);//parte entera
      lcd.print(".");
      temporal=(angulo_biela-auxiliar)*100;
      auxiliar=(long)temporal;
      lcd.print(auxiliar);
      lcd.print((char)223);
      lcd.print("    ");
    }




    //reset del encoder-----------------------------------------------------------------------------------------------------------------------
    if(digitalRead(pin_encerar_encoder) == HIGH){
      b3=digitalRead(pin_valor_encoder);
      b4=b3;
      angulo_horizontal=0;

      angulo_biela=asin(altura/biela)*180/3.141592;
      lcd.setCursor(0, 0);
      lcd.print("Alfa: ");
      auxiliar=(long)angulo_biela;
      lcd.print(auxiliar);//parte entera
      lcd.print(".");
      temporal=(angulo_biela-auxiliar)*100;
      auxiliar=(long)temporal;
      lcd.print(auxiliar);
      lcd.print((char)223);
      lcd.print("    ");
    }
    

    
    
    //LCD limpiar bandera f para que desaparezca caratula---------------------------------------------------------------------------------------
    if(f==0){
     if(millis()>t_caratula){
       lcd.clear();
       f=1;
       angulo_horizontal=0;
       angulo_biela=asin(altura/biela)*180/3.141592;
       lcd.setCursor(0, 0);
       lcd.print("Alfa: ");
       auxiliar=(long)angulo_biela;
       lcd.print(auxiliar);//parte entera
       lcd.print(".");
       temporal=(angulo_biela-auxiliar)*100;
       auxiliar=(long)temporal;
       lcd.print(auxiliar);
       lcd.print((char)223);
       lcd.print("    ");
     }
    }

}while(1);
   
}

