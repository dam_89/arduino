//https://naylampmechatronics.com/blog/25_tutorial-trasmisor-de-celda-de-carga-hx711-ba.html
//https://github.com/bogde/HX711

#include <HX711.h>

#define LOADCELL_DOUT_PIN  A1
#define LOADCELL_SCK_PIN  A0

HX711 loadcell;
float lb, kg;

void setup() {


  loadcell.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
  //loadcell.set_scale(LOADCELL_DIVIDER);
  //loadcell.set_offset(LOADCELL_OFFSET);
  
  Serial.begin(9600);
  Serial.print("Lectura del valor del ADC:  ");
  Serial.println(loadcell.read());
  Serial.println("No ponga ningun  objeto sobre la balanza");
  Serial.println("Destarando...");
  loadcell.set_scale(); //La escala por defecto es 1
  loadcell.tare(100);  //El peso actual es considerado Tara.
  Serial.println("Coloque un peso conocido:");

}

// para calcular la escala se saca el promedio de los valores leido (con get_value)y luego se divide para el peso real, esa es la escala

void loop() {

  Serial.print("Lectura: ");
  Serial.println(loadcell.get_value(10),0);
//  lb = loadcell.get_units(10);
//  kg = lb * 0.453592;
//  Serial.print("Valor [kg]:  ");
//  Serial.println(kg,3);
//  Serial.print("Valor [lb]:  ");
//  Serial.println(lb,3);

  
  //delay(10);
}
