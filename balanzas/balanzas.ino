//https://naylampmechatronics.com/blog/25_tutorial-trasmisor-de-celda-de-carga-hx711-ba.html
//https://github.com/bogde/HX711

#include <HX711.h>
#include <LiquidCrystal.h>

HX711 loadcell;
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);  // lcd(RS, E, D4, D5, D6, D7)

#define LOADCELL_DOUT_PIN  A1
#define LOADCELL_SCK_PIN  A0
//#define escala 1
//#define offset 0

void setup()
{
  // LCD
  lcd.begin(16, 2);
  lcd.clear();
  lcd.home();
  lcd.print("    BALANZAS");
  lcd.setCursor ( 0, 1 );
  lcd.print("    INKLERSA"); // nombre empresa

  // HX711
  loadcell.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
  loadcell.set_scale(-29466.771); //La escala sin parametros es 1 
  //loadcell.set_offset(offset);
  loadcell.tare(100);  //El peso actual es considerado Tara.

  // LCD
  lcd.clear();
  lcd.home();
  lcd.print("kg");
}
 
void loop()
{
  //lcd.setCursor ( 0, 1 );
  //lcd.print("                ");
  lcd.setCursor ( 0, 1 );
  lcd.print(loadcell.get_units(20));  
}
