// PARA ARDUINO NANO

//https://naylampmechatronics.com/blog/25_tutorial-trasmisor-de-celda-de-carga-hx711-ba.html
//https://github.com/bogde/HX711

#include <HX711.h>
#include <LiquidCrystal.h>

HX711 loadcell;
LiquidCrystal lcd(7, 6, 5, 4, 3, 2);  // lcd(RS, E, D4, D5, D6, D7) //***M

#define LOADCELL_DOUT_PIN  A1 //***M
#define LOADCELL_SCK_PIN  A0 //***M
//#define escala 1
//#define offset 0

//variables globales
String unidad = "Kg";
double multiplicador = 0.001; // 0.0001 para kg
double acum = 0, valor = 0;
int counter = 0;
#define iteraciones 10 //***M 
#define iteraciones_tare 56 //***M
#define k_mode 12 //***M
#define k_tare 11 //***M
#define k_zero 10 //***M
char keys[3] = "000";  //sirven como flags para que al presionar solo tenga efecto la accion 1 vez

void setup()
{
  // LCD
  lcd.begin(16, 2);
  lcd.clear();
  lcd.home();
  lcd.print("    BALANZAS");
  lcd.setCursor ( 0, 1 );
  lcd.print("    INKLERSA"); //***M

  // HX711
  loadcell.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
  loadcell.set_scale(-44.1326199346405); //La escala sin parámetros es 1 // SIEMPRE LA ESCALA DEBE SER EN GRAMOS PARA ESTE PROGRAMA //***M
  //loadcell.set_offset(offset);
  loadcell.tare(iteraciones_tare);  //El peso actual es considerado Tara. //***M
  lcd.clear();
  lcd.home();

  //DIGITAL INPUT
  pinMode(k_mode, INPUT_PULLUP); 
  pinMode(k_tare, INPUT_PULLUP); 
  pinMode(k_zero, INPUT_PULLUP); 
  //pinMode( 9, INPUT_PULLUP);

}
 
void loop()
{
  input_actions();
  calcular();
  imprimir();
}

void input_actions(){
  //leer primera vez para disparar antirebote
  if( (digitalRead(k_mode) == LOW && keys[0] == '0') || (digitalRead(k_tare) == LOW && keys[1] == '0') || (digitalRead(k_zero) == LOW && keys[2] == '0')){
    //antirebote
    delay(70);

    //Lee nuevamente valor estable
    
    // ACCIÓN: cambiar unidades y actualizar "valor"
    if(digitalRead(k_mode) == LOW && keys[0] == '0'){
      keys[0] = '1';
      valor = valor/multiplicador;
      
      if(unidad == "Kg"){
        unidad = "Lb";
        multiplicador = 0.00220462262185; 
      }
      else{
        unidad = "Kg";
        multiplicador = 0.001;
      }
      valor = valor * multiplicador;
    }

    //ACCION: encerar >> tare y zero practicamente hacen lo mismo
    if((digitalRead(k_tare) == LOW && keys[1] == '0') || (digitalRead(k_zero) == LOW && keys[2] == '0')){
      keys[1] = '1';
      keys[2] = '1';
      lcd.clear();
      lcd.home();
      lcd.print("  Zero/Tare...");
      lcd.setCursor ( 0, 1 );
      lcd.print("                ");
      loadcell.tare(iteraciones_tare);  //El peso actual es considerado Tara.
      valor = 0;
      acum = 0;
      counter = 0;
      lcd.clear();
    }
  }

  //limpiar flags cuando se sueltan las teclas
  if(digitalRead(k_mode) == HIGH && keys[0] == '1'){ keys[0] = '0';}
  if(digitalRead(k_tare) == HIGH && keys[1] == '1'){ keys[1] = '0';}
  if(digitalRead(k_zero) == HIGH && keys[2] == '1'){ keys[2] = '0';}
  
}

// promediar
void calcular(){
    acum += loadcell.get_units();
    counter++;
    if(counter==iteraciones){
      valor = (acum / counter) * multiplicador;
      acum = 0;
      counter = 0;
    }
}

//imprimir adecuadamente en el display 16x2
void imprimir(){
  lcd.home();

  //plantillas
  lcd.print("UNIDAD:");
  lcd.setCursor ( 12, 0 );
  lcd.print("["+unidad+"]");
  lcd.setCursor ( 0, 1 );
  lcd.print("PESO:");

  //signo
  lcd.setCursor (6,1);
  if(valor<0){
    lcd.print("(-)");  
  }
  else{
    lcd.print("   ");
  }

  //alinear a la derecha el valor numerico
  double _valor = abs(valor);
  int _off = 0;

  lcd.setCursor ( 9, 1 );
  if(_valor<100){_off=1; lcd.print(" ");}
  if(_valor<10) {_off=2; lcd.print(" ");}
  
  lcd.setCursor ( 9 + _off, 1 );
  lcd.print(_valor,3);
}
