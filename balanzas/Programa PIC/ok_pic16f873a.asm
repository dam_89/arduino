
_main:

;ok_pic16f873a.c,18 :: 		void main()
;ok_pic16f873a.c,22 :: 		TRISB.B0 = 1; //DATA pin as input
	BSF        TRISB+0, 0
;ok_pic16f873a.c,23 :: 		TRISB.B1 = 0; //SCK pin as output
	BCF        TRISB+0, 1
;ok_pic16f873a.c,24 :: 		TRISB.B2 = 0; //SCK pin as output
	BCF        TRISB+0, 2
;ok_pic16f873a.c,25 :: 		PORTB.B2=1;
	BSF        PORTB+0, 2
;ok_pic16f873a.c,27 :: 		UART1_Init(9600);
	MOVLW      25
	MOVWF      SPBRG+0
	BSF        TXSTA+0, 2
	CALL       _UART1_Init+0
;ok_pic16f873a.c,28 :: 		Delay_ms(100);
	MOVLW      130
	MOVWF      R12+0
	MOVLW      221
	MOVWF      R13+0
L_main0:
	DECFSZ     R13+0, 1
	GOTO       L_main0
	DECFSZ     R12+0, 1
	GOTO       L_main0
	NOP
	NOP
;ok_pic16f873a.c,35 :: 		for(;;)
L_main1:
;ok_pic16f873a.c,38 :: 		if(UART1_Data_Ready())
	CALL       _UART1_Data_Ready+0
	MOVF       R0+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main4
;ok_pic16f873a.c,41 :: 		value=UART1_Read();
	CALL       _UART1_Read+0
	MOVF       R0+0, 0
	MOVWF      _value+0
;ok_pic16f873a.c,44 :: 		if(value=='F')
	MOVF       R0+0, 0
	XORLW      70
	BTFSS      STATUS+0, 2
	GOTO       L_main5
;ok_pic16f873a.c,46 :: 		PORTB.B2=0;
	BCF        PORTB+0, 2
;ok_pic16f873a.c,47 :: 		}
L_main5:
;ok_pic16f873a.c,49 :: 		if(value=='O')
	MOVF       _value+0, 0
	XORLW      79
	BTFSS      STATUS+0, 2
	GOTO       L_main6
;ok_pic16f873a.c,51 :: 		PORTB.B2=1;
	BSF        PORTB+0, 2
;ok_pic16f873a.c,52 :: 		}
L_main6:
;ok_pic16f873a.c,54 :: 		if(value=='R')
	MOVF       _value+0, 0
	XORLW      82
	BTFSS      STATUS+0, 2
	GOTO       L_main7
;ok_pic16f873a.c,56 :: 		PORTB.B2=1;
	BSF        PORTB+0, 2
;ok_pic16f873a.c,58 :: 		Count=0;
	CLRF       _Count+0
	CLRF       _Count+1
	CLRF       _Count+2
	CLRF       _Count+3
;ok_pic16f873a.c,62 :: 		if(HX711_SCK){
	BTFSS      RB1_bit+0, BitPos(RB1_bit+0)
	GOTO       L_main8
;ok_pic16f873a.c,63 :: 		HX711_SCK=0;   //si el sck estaba en 1, se pasa a 0 y se espera al menos 400 ms para estabilizacion
	BCF        RB1_bit+0, BitPos(RB1_bit+0)
;ok_pic16f873a.c,64 :: 		Delay_ms(490); //tiempo de espera hasta que se estabilice el chip
	MOVLW      3
	MOVWF      R11+0
	MOVLW      125
	MOVWF      R12+0
	MOVLW      89
	MOVWF      R13+0
L_main9:
	DECFSZ     R13+0, 1
	GOTO       L_main9
	DECFSZ     R12+0, 1
	GOTO       L_main9
	DECFSZ     R11+0, 1
	GOTO       L_main9
;ok_pic16f873a.c,65 :: 		}
L_main8:
;ok_pic16f873a.c,69 :: 		while(HX711_DT); //espera que hx tenga un valor listo (data ready)
L_main10:
	BTFSS      RB0_bit+0, BitPos(RB0_bit+0)
	GOTO       L_main11
	GOTO       L_main10
L_main11:
;ok_pic16f873a.c,70 :: 		Delay_us(10);
	MOVLW      3
	MOVWF      R13+0
L_main12:
	DECFSZ     R13+0, 1
	GOTO       L_main12
;ok_pic16f873a.c,73 :: 		for (i=0;i<24;i++){
	CLRF       _i+0
	CLRF       _i+1
L_main13:
	MOVLW      128
	XORWF      _i+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main23
	MOVLW      24
	SUBWF      _i+0, 0
L__main23:
	BTFSC      STATUS+0, 0
	GOTO       L_main14
;ok_pic16f873a.c,75 :: 		HX711_SCK = 1;
	BSF        RB1_bit+0, BitPos(RB1_bit+0)
;ok_pic16f873a.c,76 :: 		Delay_us(10);
	MOVLW      3
	MOVWF      R13+0
L_main16:
	DECFSZ     R13+0, 1
	GOTO       L_main16
;ok_pic16f873a.c,77 :: 		HX711_SCK = 0;
	BCF        RB1_bit+0, BitPos(RB1_bit+0)
;ok_pic16f873a.c,78 :: 		Delay_us(10);
	MOVLW      3
	MOVWF      R13+0
L_main17:
	DECFSZ     R13+0, 1
	GOTO       L_main17
;ok_pic16f873a.c,81 :: 		Count=Count<<1;
	RLF        _Count+0, 1
	RLF        _Count+1, 1
	RLF        _Count+2, 1
	RLF        _Count+3, 1
	BCF        _Count+0, 0
;ok_pic16f873a.c,82 :: 		if(HX711_DT){Count++;}
	BTFSS      RB0_bit+0, BitPos(RB0_bit+0)
	GOTO       L_main18
	MOVF       _Count+0, 0
	MOVWF      R0+0
	MOVF       _Count+1, 0
	MOVWF      R0+1
	MOVF       _Count+2, 0
	MOVWF      R0+2
	MOVF       _Count+3, 0
	MOVWF      R0+3
	INCF       R0+0, 1
	BTFSC      STATUS+0, 2
	INCF       R0+1, 1
	BTFSC      STATUS+0, 2
	INCF       R0+2, 1
	BTFSC      STATUS+0, 2
	INCF       R0+3, 1
	MOVF       R0+0, 0
	MOVWF      _Count+0
	MOVF       R0+1, 0
	MOVWF      _Count+1
	MOVF       R0+2, 0
	MOVWF      _Count+2
	MOVF       R0+3, 0
	MOVWF      _Count+3
L_main18:
;ok_pic16f873a.c,73 :: 		for (i=0;i<24;i++){
	INCF       _i+0, 1
	BTFSC      STATUS+0, 2
	INCF       _i+1, 1
;ok_pic16f873a.c,83 :: 		}
	GOTO       L_main13
L_main14:
;ok_pic16f873a.c,86 :: 		HX711_SCK = 1;
	BSF        RB1_bit+0, BitPos(RB1_bit+0)
;ok_pic16f873a.c,87 :: 		Delay_us(10);
	MOVLW      3
	MOVWF      R13+0
L_main19:
	DECFSZ     R13+0, 1
	GOTO       L_main19
;ok_pic16f873a.c,88 :: 		HX711_SCK = 0;
	BCF        RB1_bit+0, BitPos(RB1_bit+0)
;ok_pic16f873a.c,92 :: 		if( (Count & 0x00800000) > 0 )
	MOVLW      0
	ANDWF      _Count+0, 0
	MOVWF      R1+0
	MOVLW      0
	ANDWF      _Count+1, 0
	MOVWF      R1+1
	MOVLW      128
	ANDWF      _Count+2, 0
	MOVWF      R1+2
	MOVLW      0
	ANDWF      _Count+3, 0
	MOVWF      R1+3
	MOVF       R1+3, 0
	SUBLW      0
	BTFSS      STATUS+0, 2
	GOTO       L__main24
	MOVF       R1+2, 0
	SUBLW      0
	BTFSS      STATUS+0, 2
	GOTO       L__main24
	MOVF       R1+1, 0
	SUBLW      0
	BTFSS      STATUS+0, 2
	GOTO       L__main24
	MOVF       R1+0, 0
	SUBLW      0
L__main24:
	BTFSC      STATUS+0, 0
	GOTO       L_main20
;ok_pic16f873a.c,94 :: 		Count |= 0xFF000000;
	MOVLW      0
	IORWF      _Count+0, 1
	MOVLW      0
	IORWF      _Count+1, 1
	MOVLW      0
	IORWF      _Count+2, 1
	MOVLW      255
	IORWF      _Count+3, 1
;ok_pic16f873a.c,95 :: 		}
	GOTO       L_main21
L_main20:
;ok_pic16f873a.c,98 :: 		Count &= 0x00FFFFFF;
	MOVLW      255
	ANDWF      _Count+0, 1
	MOVLW      255
	ANDWF      _Count+1, 1
	MOVLW      255
	ANDWF      _Count+2, 1
	MOVLW      0
	ANDWF      _Count+3, 1
;ok_pic16f873a.c,99 :: 		}
L_main21:
;ok_pic16f873a.c,101 :: 		mb=(long)Count;
	MOVF       _Count+0, 0
	MOVWF      _mb+0
	MOVF       _Count+1, 0
	MOVWF      _mb+1
	MOVF       _Count+2, 0
	MOVWF      _mb+2
	MOVF       _Count+3, 0
	MOVWF      _mb+3
;ok_pic16f873a.c,108 :: 		LongToStr(mb, txt);
	MOVF       _Count+0, 0
	MOVWF      FARG_LongToStr_input+0
	MOVF       _Count+1, 0
	MOVWF      FARG_LongToStr_input+1
	MOVF       _Count+2, 0
	MOVWF      FARG_LongToStr_input+2
	MOVF       _Count+3, 0
	MOVWF      FARG_LongToStr_input+3
	MOVLW      _txt+0
	MOVWF      FARG_LongToStr_output+0
	CALL       _LongToStr+0
;ok_pic16f873a.c,109 :: 		UART1_Write_Text(txt); // send value to the UART
	MOVLW      _txt+0
	MOVWF      FARG_UART1_Write_Text_uart_text+0
	CALL       _UART1_Write_Text+0
;ok_pic16f873a.c,110 :: 		UART1_Write('.');
	MOVLW      46
	MOVWF      FARG_UART1_Write_data_+0
	CALL       _UART1_Write+0
;ok_pic16f873a.c,121 :: 		PORTB.B2=0;
	BCF        PORTB+0, 2
;ok_pic16f873a.c,122 :: 		}
L_main7:
;ok_pic16f873a.c,124 :: 		}
L_main4:
;ok_pic16f873a.c,125 :: 		}
	GOTO       L_main1
;ok_pic16f873a.c,126 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
