//The example will use the HX711, determine the offset and calculate the weight.

sbit HX711_DT at RB0_bit ; // Set PIN data to RB0
sbit HX711_SCK at RB1_bit; // Set PIN clock to RB1


char txt[14];



            int i=0;
            unsigned long int Count;
            long int mb=0;

            char value;
            
            
void main()
{

           
    TRISB.B0 = 1; //DATA pin as input
    TRISB.B1 = 0; //SCK pin as output
    TRISB.B2 = 0; //SCK pin as output
    PORTB.B2=1;

    UART1_Init(9600);
    Delay_ms(100);
    /*
    HX711_Init(128);
    HX711_Set_Scale(37.25); // set sensibility at 37.25 bit/gram
    HX711_Reset_Measure(20); // get offset
    */

    for(;;)
    {

        if(UART1_Data_Ready())
        {

            value=UART1_Read();
            
            
            if(value=='F')
            {
            PORTB.B2=0;
            }
            
            if(value=='O')
            {
            PORTB.B2=1;
            }

            if(value=='R')
            {
                PORTB.B2=1;

                Count=0;
                
                //HX711_Power_Up(); // set UP the HX711

                if(HX711_SCK){
                              HX711_SCK=0;   //si el sck estaba en 1, se pasa a 0 y se espera al menos 400 ms para estabilizacion
                              Delay_ms(490); //tiempo de espera hasta que se estabilice el chip
                }
                
                //--------------------------------------------------------------------
                //Lectura
                while(HX711_DT); //espera que hx tenga un valor listo (data ready)
                Delay_us(10);

                //A�ade al unsigned long los 24bits recibidos del hx711
                for (i=0;i<24;i++){
                    //Pulso del SCK
                    HX711_SCK = 1;
                    Delay_us(10);
                    HX711_SCK = 0;
                    Delay_us(10);
                    
                    //Leer bit del DT
                    Count=Count<<1;
                    if(HX711_DT){Count++;}
                }

                // Pulso numero 25 del SCK, indica ganancia 128 en el canal A
                HX711_SCK = 1;
                Delay_us(10);
                HX711_SCK = 0;
                
                
                //Transformar de two's complement 24bits a two's complement de 32bits y grabarlo en signed long int para que sean validos los datos
                if( (Count & 0x00800000) > 0 )
                {
                    Count |= 0xFF000000;
                }
                else
                {
                    Count &= 0x00FFFFFF;
                }
    
                mb=(long)Count;

                //--------------------------------------------------------------------
                
                

                //Imprimir
                LongToStr(mb, txt);
                UART1_Write_Text(txt); // send value to the UART
                UART1_Write('.');



               /*
                //HX711_Power_Down();
                HX711_SCK=0; //solo en caso de que sck haya estado en high
                HX711_SCK=1; //despues de poner en high el sck se espera al menos 60us para "sleep mode"
                Delay_us(70);
                
                  */
                PORTB.B2=0;
            }

        }
    }
}











    /*

#define HX711_DO  RB0_bit
#define HX711_CLK RB1_bit

int32 measurement(void);

void main()
{
    int test=0;
    unsigned long int Count,BUFFER[30],offseeet=0,mied=0;
    unsigned short int i=0;


    while(1){

        while(i<10){
            Count=measurement();
            BUFFER[i]=Count;
            mied+=BUFFER[i];
            i++;
        }
        
        i=0;
        mied/=11;

        if(test==0){
          offseeet=mied;
          test=1;
        }
        //Count=measurement();
        mied-=offseeet;
        //printf("data=%lu \n\r",Count);
        UART1_Write_Text(mied);
        //printf("mied=%lu \n\r",mied);
        //delay_ms(1);
        //printf("\f");
    }
}



long int measurement(void)
{
    
    unsigned long int Count;
    unsigned short int i,A_1,A_2,A_3;


    HX711_DO = 1;
    HX711_CLK = 0;
    Count=0;

    while(HX711_DO);

    for (i=0;i<24;i++){// gain 128
        HX711_CLK = 1;
        Count=Count<<1;
        HX711_CLK = 0;
        if(HX711_DO) Count++;
    }
    HX711_CLK = 1;
    Count=Count^0x800000;
    HX711_CLK = 0;
    
    //************************
    A_1=MAKE8(Count, 0);
    A_2=MAKE8(Count, 1);
    A_3=MAKE8(Count, 2);
    A_2=(A_2 & 0b11111000);
    Count= MAKE16(A_3, A_2);
    return(Count);

}


*/