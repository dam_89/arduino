#include <SoftwareSerial.h>
#include <DFPlayer_Mini_Mp3.h>

byte msg[1]; 

int ldr1;
int ldr2;
int ldr3;
int ldr4;
int ldr5;
int ldr6;
int ldr7;
int ldr8;
int ldr9;
int ldr10;
int ldr11;
int ldr12;
int ldr13;
int ldr14;
int ldr15;
int ldr16;

long ldr1_1;
long ldr2_1;
long ldr3_1;
long ldr4_1;
long ldr5_1;
long ldr6_1;
long ldr7_1;
long ldr8_1;
long ldr9_1;
long ldr10_1;
long ldr11_1;
long ldr12_1;
long ldr13_1;
long ldr14_1;
long ldr15_1;
long ldr16_1;

byte f1=0;
byte f2=0;
byte f3=0;
byte f4=0;
byte f5=0;
byte f6=0;
byte f7=0;
byte f8=0;
byte f9=0;
byte f10=0;
byte f11=0;
byte f12=0;
byte f13=0;
byte f14=0;
byte f15=0;
byte f16=0;


long nivel_luz=0;   // poner un numero del 0-100 que indique el nivel de luminosidad
int  volumen=15;    // volumen 0-30


SoftwareSerial mySerial(10, 11); // RX, TX


void setup() {

  mySerial.begin(9600);
  Serial.begin(9600);
  mp3_set_serial (mySerial);  //set Serial for DFPlayer-mini mp3 module 
  delay(1);  //wait 1ms for mp3 module to set volume
  mp3_set_volume (volumen); //0-30
  
  pinMode(A0,INPUT);
  pinMode(A1,INPUT);
  pinMode(A2,INPUT);
  pinMode(A3,INPUT);
  pinMode(A4,INPUT);
  pinMode(A5,INPUT);
  pinMode(A6,INPUT);
  pinMode(A7,INPUT);
  pinMode(A8,INPUT);
  pinMode(A9,INPUT);
  pinMode(A10,INPUT);
  pinMode(A11,INPUT);
  pinMode(A12,INPUT);
  pinMode(A13,INPUT);
  pinMode(A14,INPUT);
  pinMode(A15,INPUT);  
  
}

void loop() {
/*
  ldr1 = analogRead(A0);
  ldr2 = analogRead(A1);
  ldr3 = analogRead(A2);
  ldr4 = analogRead(A3);
  ldr5 = analogRead(A4);
  ldr6 = analogRead(A5);
  ldr7 = analogRead(A6);
  ldr8 = analogRead(A7);
  ldr9 = analogRead(A8);
  ldr10 = analogRead(A9);
  ldr11 = analogRead(A10);
  ldr12 = analogRead(A11);
  ldr13 = analogRead(A12);
  ldr14 = analogRead(A13);
  ldr15 = analogRead(A14);
  ldr16 = analogRead(A15);

  ldr1_1 = ((long)ldr1 * 100) / 1023;
  ldr2_1 = ((long)ldr2 * 100) / 1023;
  ldr3_1 = ((long)ldr3 * 100) / 1023;
  ldr4_1 = ((long)ldr4 * 100) / 1023;
  ldr5_1 = ((long)ldr5 * 100) / 1023;
  ldr6_1 = ((long)ldr6 * 100) / 1023;
  ldr7_1 = ((long)ldr7 * 100) / 1023;
  ldr8_1 = ((long)ldr8 * 100) / 1023;
  ldr9_1 = ((long)ldr9 * 100) / 1023;
  ldr10_1 = ((long)ldr10 * 100) / 1023;
  ldr11_1 = ((long)ldr11 * 100) / 1023;
  ldr12_1 = ((long)ldr12 * 100) / 1023;
  ldr13_1 = ((long)ldr13 * 100) / 1023;
  ldr14_1 = ((long)ldr14 * 100) / 1023;
  ldr15_1 = ((long)ldr15 * 100) / 1023;
  ldr16_1 = ((long)ldr16 * 100) / 1023;



//  Serial.println(total);
//  delay(400);

/////////////////////////////////////////////////////////
  if((ldr1_1>=nivel_luz)&&(f1==0)){
    f1=1;
    Serial.println("SONIDO-1"); // esto solo sirve para comprobar en la compu.. esto se puede borrar
    mp3_play (1); 
    //delay(200000);
    //f1=0;
  }
  
////////////////////////////////////////////////////                           
*/


//leer bytes individuales

if (Serial.available() > 0) { Serial.readBytesUntil(10,msg,1); Serial.print("NUM: "); Serial.println(msg[0]);}

//mp3
if(msg[0]==49)
{
  msg[0]=0;
  mp3_play (1);
}

if(msg[0]==50)
{
  msg[0]=0;
  mp3_play (2);
}

if(msg[0]==51)
{
  msg[0]=0;
  mp3_play (3);
}

if(msg[0]==48)
{
  msg[0]=0;
  mp3_stop();
}
//leer strings
/*
while(Serial.available()) 
  {
  
  msg= Serial.readString();// read the incoming data as string
  
  Serial.println(msg);
  
  }
*/



}//////////////////////////FIN



/*
   mp3_play ();    //start play
   mp3_play (5);  //play "mp3/0005.mp3"
   mp3_next ();   //play next 
   mp3_prev ();   //play previous
   mp3_set_volume (uint16_t volume);  //0~30
   mp3_set_EQ (); //0~5
   mp3_pause ();
   mp3_stop ();
   void mp3_get_state ();   //send get state command
   void mp3_get_volume (); 
   void mp3_get_u_sum (); 
   void mp3_get_tf_sum (); 
   void mp3_get_flash_sum (); 
   void mp3_get_tf_current (); 
   void mp3_get_u_current (); 
   void mp3_get_flash_current (); 
   void mp3_single_loop (boolean state);  //set single loop 
   void mp3_DAC (boolean state); 
   void mp3_random_play (); 
 */
