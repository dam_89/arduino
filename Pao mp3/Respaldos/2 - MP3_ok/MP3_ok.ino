/////////////////////////////////////////// ENCABEZADO ////////////////////////////////////////

#include "SoftwareSerial.h"
# define Start_Byte 0x7E
# define Version_Byte 0xFF
# define Command_Length 0x06
# define Acknowledge 0x00 //Returns info with command 0x41 [0x01: info, 0x00: no info]
# define End_Byte 0xEF

# define busy1 2 //pin2
# define busy2 3 //pin3
# define busy3 4 //pin4

# define B_PLAY 28 //pin 28, etc
# define B_NEXT 30
# define B_PREV 32
# define B_SELE 34
# define B_MODE 36

# define L_MANU 38
# define L_AUTO 40
# define L_REP1 42
# define L_REP2 44
# define L_REP3 46
   

SoftwareSerial mySerial_1(10, 11); // TX, RX
SoftwareSerial mySerial_2(12, 13);
SoftwareSerial mySerial_3(50, 52);


/////////////////////////////////// VARIABLES GLOBALES ////////////////////////////////

int inChar, num=0, df=1; //auxiliares para lectura de monitor y ejecucion de comandos mp3
String inString=""; //auxiliar de lectura de monitor
byte i=0,j=0; //auxiliar para bucles for y while
byte ldr[16]; //En este array "ldr" se guardaran las posiciones de los sensores activos
byte rep[3]={21,21,21}; //array que indica que sonido se esta reproduciendo en cada dispositivo
byte q=0; //cantidad de sensores activos
byte h=0; //numero de reproductor disponible
byte mode=1; //0=modo manual 1=modo auto( deteccion de sensores )
byte mp3=1; //indica el reproductor seleccionado en el modo manual
byte flags[5]={0,0,0,0,0}; //flags para pulsadores


///////////////////////////////////// PARAMETROS /////////////////////////////////////////

int nivel_luz[16] = {50,50,50,50,50,50,50,50,50,50,50,50,50,50,50,50};   // poner un numero del 0-100 que indique el nivel de luminosidad para activar sonidos
int volumen = 15;    // volumen 0-30


//////////////////////////////////// SETUP //////////////////////////////////////////////////

void setup () {

  pinMode(busy1, INPUT);
  pinMode(busy2, INPUT);
  pinMode(busy3, INPUT);  
  
  pinMode(B_PLAY, INPUT_PULLUP);
  pinMode(B_NEXT, INPUT_PULLUP);
  pinMode(B_PREV, INPUT_PULLUP);
  pinMode(B_SELE, INPUT_PULLUP);
  pinMode(B_MODE, INPUT_PULLUP);

  pinMode(L_MANU, OUTPUT); digitalWrite(L_MANU, LOW);
  pinMode(L_AUTO, OUTPUT); digitalWrite(L_AUTO, HIGH);
  pinMode(L_REP1, OUTPUT); digitalWrite(L_REP1, LOW);
  pinMode(L_REP2, OUTPUT); digitalWrite(L_REP2, LOW);
  pinMode(L_REP3, OUTPUT); digitalWrite(L_REP3, LOW);

  pinMode(A0,INPUT);
  pinMode(A1,INPUT);
  pinMode(A2,INPUT);
  pinMode(A3,INPUT);
  pinMode(A4,INPUT);
  pinMode(A5,INPUT);
  pinMode(A6,INPUT);
  pinMode(A7,INPUT);
  pinMode(A8,INPUT);
  pinMode(A9,INPUT);
  pinMode(A10,INPUT);
  pinMode(A11,INPUT);
  pinMode(A12,INPUT);
  pinMode(A13,INPUT);
  pinMode(A14,INPUT);
  pinMode(A15,INPUT);
 
  Serial.begin(9600);       delay(100);  
  mySerial_1.begin (9600);  delay(100);  Inicializar(1);
  mySerial_2.begin (9600);  delay(100);  Inicializar(2);
 // mySerial_3.begin (9600);  delay(100);  Inicializar(3);

  stopp(2); //stopp provoca un delay, se usa aprovecha para antirebote
  stopp(3);
  play_system(1);  
  Serial.println("Sistema Activado!");
}

//////////////////////////////////// LOOP //////////////////////////////////////////////////

void loop () { 

  //--------------------------------------COMUNICACION SERIAL MONITOR-----------------------------------
  //MONITOR SERIE INPUT
  while (Serial.available() > 0) 
  {
      inChar = Serial.read();
      if (isDigit(inChar)) 
      {
        inString += (char)inChar;
      }
      else
      {
        if(inChar==97){df=1;} //Si se ingresa "a" 
        if(inChar==98){df=2;} //Si se ingresa "b"
        if(inChar==99){df=3;} //Si se ingresa "c"
        Serial.print("Reproductor seleccionado: "); Serial.println(df);
      }
      
      if (inChar == '\n') 
      {
        Serial.print("Valor Ingresado:");
        num=inString.toInt();
        Serial.println(num);
        inString = "";
      }
  }


  //EJECUTAR COMANDO RECIBIDO DEL MONITOR
  if(num!=0)
  {
    if(num <100){ play_track(df,num); }
    if(num==100){ play(df);  }
    if(num==200){ pause(df); }
    if(num==300){ stopp(df); }
    num=0;
  }


  //--------------------------------------SELECCIONAR MODO-----------------------------------
  
  if((flags[4]==0) && (digitalRead(B_MODE)==0))
  {
    flags[4]=1;
    digitalWrite(L_MANU, LOW);
    digitalWrite(L_AUTO, LOW);
    digitalWrite(L_REP1, LOW);
    digitalWrite(L_REP2, LOW);
    digitalWrite(L_REP3, LOW);
    stopp(2); //stopp provoca un delay, se usa aprovecha para antirebote
    stopp(3);
    if(mode==0)
    {
      mode=1;
      play_system(2); 
      digitalWrite(L_AUTO, HIGH);
    }
    else
    {
      mode=0;
      play_system(3);
      digitalWrite(L_MANU, HIGH);
      digitalWrite(L_REP1, HIGH);
      mp3=1;
    }     
  }

  //Reset flag
  if((flags[4]==1) && (digitalRead(B_MODE)==1))
  {
    delay(70); 
    flags[4]=0;
  }


  
  //--------------------------------------MODO MANUAL-----------------------------------
  if(mode==0) 
  {
    if((flags[0]==0) && (digitalRead(B_PLAY)==0))
    {
      delay(70); flags[0]=1;
      if(mp3==1){ if(digitalRead(busy1)==0){pause(1);}else{play(1);} }
      if(mp3==2){ if(digitalRead(busy2)==0){pause(2);}else{play(2);} }
      if(mp3==3){ if(digitalRead(busy3)==0){pause(3);}else{play(3);} }
    }
    
    if((flags[1]==0) && (digitalRead(B_NEXT)==0))
    {
      delay(70); flags[1]=1;
      if((mp3==1)&&(digitalRead(busy1)==0)){ next(1); }
      if((mp3==2)&&(digitalRead(busy2)==0)){ next(2); }
      if((mp3==3)&&(digitalRead(busy3)==0)){ next(3); }
    }
    
    if((flags[2]==0) && (digitalRead(B_PREV)==0))
    {
      delay(70); flags[2]=1;
      if((mp3==1)&&(digitalRead(busy1)==0)){ prev(1); }
      if((mp3==2)&&(digitalRead(busy2)==0)){ prev(2); }
      if((mp3==3)&&(digitalRead(busy3)==0)){ prev(3); }      
    }
    
    if((flags[3]==0) && (digitalRead(B_SELE)==0))
    {
      delay(70); flags[3]=1;
      digitalWrite(L_REP1, LOW);
      digitalWrite(L_REP2, LOW);
      digitalWrite(L_REP3, LOW);  
      mp3++;
      if(mp3>3){mp3=1;}
      if(mp3==1){ digitalWrite(L_REP1, HIGH); }
      if(mp3==2){ digitalWrite(L_REP2, HIGH); }
      if(mp3==3){ digitalWrite(L_REP3, HIGH); }
    }

  }

  //Reset flags
  if((flags[0]==1) && (digitalRead(B_PLAY)==1)) { delay(70); flags[0]=0; }
  if((flags[1]==1) && (digitalRead(B_NEXT)==1)) { delay(70); flags[1]=0; }
  if((flags[2]==1) && (digitalRead(B_PREV)==1)) { delay(70); flags[2]=0; }
  if((flags[3]==1) && (digitalRead(B_SELE)==1)) { delay(70); flags[3]=0; }



  //--------------------------------------MODO AUTOMATICO-----------------------------------
  
  if(mode==1) 
  {
    //REPRODUCIR SONIDOS
    if(ActualizarArray()>0)
    {
      while( (BuscarLibre()>0) && (q>0) )
      {
        //Reproduce al azar un sonido asociado a un sensor
        j=random(0,q);
        play_track(h,ldr[j]+1);
  
        //Almacena posicion de X sonido, se reproduce en Y dispositivo
        rep[h-1]=ldr[j];
  
        //elimino el que ya se reprodujo
        q--;
        for(i=j; i<q; i++){ ldr[i]=ldr[i+1]; }
        ldr[q]=21;      
      }
    }
    //limipiar arrayrep
    LimpiarR(); 
  }
  
}





//////////////////////////////////// FUNCIONES PRINCIPALES //////////////////////////////////////////////////

byte ActualizarArray()
{
  //limpiar 
  for(i=0; i<16; i++){ ldr[i]=21; } 

  //Guardar en el Array LDR las posiciones de los sensores activos, excepto de los que actualmente se estan reproduciendo
  q=0;
  for(i=0; i<16; i++) 
  {
    if( (  (int)(((float)analogRead(i)*100)/1023)  >=nivel_luz[i]) && (i!=rep[0]) && (i!=rep[1]) && (i!=rep[2]))
    { 
      ldr[q] = i;
      q++;
    } 
  }
  return q;  //devuelve la cantidad de sensores activos
}

byte BuscarLibre()
{
  //Buscar un dispositivo libre
  h=0;
  if(digitalRead(busy3)==1){ h=3; }  //busy... 0=playing 1=stopped
  if(digitalRead(busy2)==1){ h=2; }
  if(digitalRead(busy1)==1){ h=1; }  
  return h; //en caso de estar libre ninguno, devuelve 0  
}

void LimpiarR()
{
  if((rep[0]!=21) && (digitalRead(busy1)==1)){ rep[0]=21; }  //busy... 0=playing 1=stopped
  if((rep[1]!=21) && (digitalRead(busy2)==1)){ rep[1]=21; }
  if((rep[2]!=21) && (digitalRead(busy3)==1)){ rep[2]=21; }  
}



//////////////////////////////////// FUNCIONES COMUNICACION //////////////////////////////////////////////////


void Inicializar(byte disp) //inicializa un dfplayermini. Existen solamente 3 en este proyecto
{
  execute_CMD(disp, 0x3F, 0x00, 0x00);   // Send request for initialization parameters
  if(disp==1){ while (mySerial_1.available()<10){} } // Wait until initialization parameters are received (10 bytes)
  if(disp==2){ while (mySerial_2.available()<10){} }
  if(disp==3){ while (mySerial_3.available()<10){} }    
  delay(100);
  setVolume(disp, volumen);  
}

void play_system(byte n)
{
  execute_CMD(1,0x0F,30,n); //folder "30", file n
  while(digitalRead(busy1)==0){}
  execute_CMD(1,0x4E,0,1); //regresa a la primera carpeta "01" a 001.mp3
}

void play_track(byte disp, byte n)
{
  execute_CMD(disp,0x0F,1,n); //folder "01", file n
}

void play(byte disp)
{
  execute_CMD(disp,0x0D,0,0); 
}

void pause(byte disp)
{
  execute_CMD(disp,0x0E,0,0);
}

void stopp(byte disp)
{
  execute_CMD(disp,0x16,0,0);
}

void next(byte disp)
{
  execute_CMD(disp,0x01,0,0); 
}

void prev(byte disp)
{
  execute_CMD(disp,0x02,0,0); 
}

void setVolume(byte disp, byte volume)
{
  if(volume>30){volume=30;}
  execute_CMD(disp, 0x06, 0, volume); // Set the volume (0x00~0x30)
}

void execute_CMD(byte disp, byte CMD, byte Par1, byte Par2)
{
  word checksum = -(Version_Byte + Command_Length + CMD + Acknowledge + Par1 + Par2);
  byte Command_line[10] = { Start_Byte, Version_Byte, Command_Length, CMD, Acknowledge, Par1, Par2, highByte(checksum), lowByte(checksum), End_Byte};
  WriteSerial(disp,Command_line);
  delay(100); 
}

void WriteSerial(byte disp, byte cline[10])
{
  if(disp==1){ for(i=0; i<10; i++){ mySerial_1.write(cline[i]);} }
  if(disp==2){ for(i=0; i<10; i++){ mySerial_2.write(cline[i]);} }
  if(disp==3){ for(i=0; i<10; i++){ mySerial_3.write(cline[i]);} }
}


//////////////////////////////// COMENTARIOS EXTRAS ///////////////////////////////////

/*
ESTOS SON TESTS (no probados al 100%) PARA LEER LOS DATOS ENVIADOS POR EL REPRODUCTOR REFERENTE A SU STATUS
SIN EMBARGO EN ESTE PROYECTO NO SON NECESARIOS POR USAR EL TERMINAL BUSY DEL REPRODUCTOR MP3
 //Recibir datos en monitor del 1er DFPlayer
  if(mySerial_1.isListening()){ mySerial_1.listen();
  while (mySerial_1.available()>=10)
  {
    // There is at least 1 returned message (10 bytes each)   
    // Read the returned code
    byte Returned[10];
    for (byte k=0; k<10; k++)
      Returned[k] = mySerial_1.read();
    
    // Write the returned code to the screen
    Serial.print("Returned 1: 0x"); if (Returned[3] < 16) Serial.print("0"); Serial.print(Returned[3],HEX);
    Serial.print("("); Serial.print(Returned[3], DEC); 
    Serial.print("); Parameter: 0x"); if (Returned[5] < 16) Serial.print("0"); Serial.print(Returned[5],HEX);
    Serial.print("("); Serial.print(Returned[5], DEC); 
    Serial.print("), 0x"); if (Returned[6] < 16) Serial.print("0"); Serial.print(Returned[6],HEX);
    Serial.print("("); Serial.print(Returned[6], DEC); Serial.println(")");
  }}

//Recibir datos en monitor del 1er DFPlayer
  //mySerial_2.listen();
  if(mySerial_2.isListening()){mySerial_2.listen();
  while (mySerial_2.available()>=10)
  {
    // There is at least 1 returned message (10 bytes each)   
    // Read the returned code
    byte Returned2[10];
    for (byte k2=0; k2<10; k2++)
      Returned2[k2] = mySerial_2.read();
    
    // Write the returned code to the screen
    Serial.print("Returned 2: 0x"); if (Returned2[3] < 16) Serial.print("0"); Serial.print(Returned2[3],HEX);
    Serial.print("("); Serial.print(Returned2[3], DEC); 
    Serial.print("); Parameter: 0x"); if (Returned2[5] < 16) Serial.print("0"); Serial.print(Returned2[5],HEX);
    Serial.print("("); Serial.print(Returned2[5], DEC); 
    Serial.print("), 0x"); if (Returned2[6] < 16) Serial.print("0"); Serial.print(Returned2[6],HEX);
    Serial.print("("); Serial.print(Returned2[6], DEC); Serial.println(")");
  }}

*/

