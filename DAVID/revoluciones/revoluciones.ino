#include <NewPing.h>
#include <SoftwareSerial.h>   // Incluimos la librería  SoftwareSerial  
SoftwareSerial BT(10,11);    // Definimos los pines RX y TX del Arduino conectados al Bluetooth
 
volatile int contador = 0;   // Variable entera que se almacena en la RAM del Micro
//ultrasonico
int pinEcho = 6;   
int pinTrig = 4; 
int max_distance = 200; // Distancia máxima a detectar en cm
 
NewPing sonar(pinTrig, pinEcho, max_distance);
void setup() {
  BT.begin(38400);       // Inicializamos el puerto serie BT (Para Modo AT 2)
  //Serial.begin(9600);   // Inicializamos  el puerto serie  
  Serial.begin(57600);
  attachInterrupt(0,interrupcion0,RISING);  // Interrupcion 0 (pin2) 
}                                          // LOW, CHANGE, RISING, FALLING
 
void loop() {
  delay(999);               // retardo de casi 1 segundo
  Serial.print(contador*30); // Como son dos interrupciones por vuelta (contador * (60/2))
  Serial.println(" RPM");    //  El numero 2 depende del numero aspas de la helise del motor en prueba
  Serial.print((contador*30)*0.020); // Como son dos interrupciones por vuelta (contador * (60/2))
  Serial.println(" m/s"); 
  
  BT.write("A");
  BT.write(contador*30);
  
  BT.write("B");
  BT.write((contador*30)*0.020);
  
  contador = 0;
  int distance = sonar.ping_cm();
  if(distance > 0) { 
    
    Serial.print("Distancia "); 
    Serial.print(distance); 
    Serial.println(" cm"); 
    BT.write("C");
    BT.write(distance);
    
    delay(1000);
  } else { 
    
    Serial.println("Distancia desconocida"); 
    delay(1000);
  }
}


//A RPM
//B M/s
//C M
 
void interrupcion0()    // Funcion que se ejecuta durante cada interrupion
{
  contador++;           // Se incrementa en uno el contador
}
