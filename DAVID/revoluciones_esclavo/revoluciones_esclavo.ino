#include <SoftwareSerial.h>   // Incluimos la librería  SoftwareSerial  
SoftwareSerial BT(10,11);    // Definimos los pines RX y TX del Arduino conectados al Bluetooth

#include <LiquidCrystal.h> // includes the LiquidCrystal Library 
LiquidCrystal lcd(14,2,6,5,4,3);
//LiquidCrystal lcd(12,13,5,4,3,9);
char c;
int y, r, ys, ps, ff;
float p;


void setup()
{      
BT.begin(9600);
Serial.begin(9600);
lcd.clear();
lcd.begin(16,2);
}

void loop() 
{

  if(BT.available())
  {
    //lcd.clear();
    ys=BT.read();      
    if (ys == 'A'){
      
      y=BT.parseInt();
      Serial.print("RPM  :   ");
      Serial.println(y);
      lcd.setCursor(0,0);
      lcd.print("RPM ");
      p=(y*0.020);
      lcd.print("            ");
      lcd.setCursor(4,0);
      lcd.print(y);
      lcd.setCursor(0,1);
      lcd.print("V ");
      lcd.print("      ");
      lcd.setCursor(2,1);
      lcd.print(p);
      
      //Serial.print("VEL  :   ");
      //Serial.println(p);
      //lcd.setCursor(1,0);
      //lcd.print(p);
      //delay(100);
      
    }


     if (ys == 'C'){

      r=BT.parseInt();
      //Serial.print("D   :   ");
      //Serial.println(r);
      //lcd.setCursor(5,0);
      //lcd.print(r);
      //delay(100);
      lcd.setCursor(8,1);
      lcd.print("cm ");
      lcd.print("        ");
      lcd.setCursor(11,1);
      lcd.print(r);
    }
    }
    
  }
